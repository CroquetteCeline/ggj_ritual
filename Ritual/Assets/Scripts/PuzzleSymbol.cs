﻿using UnityEngine;
using System.Collections;

public enum Symbol
{
    None,
    Default,

    NoHand,
    Leafs,
    Rock,
    Happy,
    NotHappy,
    Money,
    Question,
    Exclamation,
    Carrot,
    Angry,
    Weights,
    Hearth,

}


public class PuzzleSymbol : MonoBehaviour
{
    public Symbol _eType;
    //public bool _bClickable = false;
    //public bool _bDragable = false;
    public int _iUsedOnTurn = 0;

    bool _bDragged = false;
    public float _fFollowForce = 3.5f;
    static float _fLineWidth = 0.1f;
    public Material _tMat;

    Rigidbody2D _tRigidbody;
    Collider2D _tCollider;
    Collider2D _tNoDrag;
    LineRenderer _tLine;
    Transform _tBuddy;

    public AnimationCurve _tScaleDown;

    void Start ()
    {
        _tRigidbody = GetComponent<Rigidbody2D>();
        _tCollider = GetComponent<Collider2D>();
        //_tNoDrag = GameObject.Find("NoDragArea").GetComponent<Collider2D>();
    }
	
	void Update ()
    {
        if (_tLine != null)
        {
            _tLine.SetPosition(0, transform.position - Vector3.forward);
            _tLine.SetPosition(1, _tBuddy.position - Vector3.forward);
        }
    }

    public void CreateLink(GameObject tBuddy, Mood eMood )
    {
        int iSnapValue = 0;

        switch (eMood)
        {           
            case Mood.Happy:
                iSnapValue = 3;
                break;
            case Mood.Okay:
                iSnapValue = 1;
                break;
            default:
                return;
        }

        Debug.Log("Snap__" + iSnapValue);
        _tBuddy = tBuddy.transform;

        //gameObject.AddComponent<RelativeJoint2D>();
        SpringJoint2D tJ = gameObject.AddComponent<SpringJoint2D>();
        tJ.connectedBody = tBuddy.GetComponent<Rigidbody2D>();
        tJ.distance = (tBuddy.transform.position - transform.position).magnitude;
        tJ.enableCollision = true;
        tJ.frequency = iSnapValue * 0.3f;
        tJ.dampingRatio = 0.2f;

        _tLine = gameObject.AddComponent<LineRenderer>();
        _tLine.useWorldSpace = true;
        _tLine.SetVertexCount(2);
        Debug.Log(_tMat);
        _tLine.material = _tMat;
        _tLine.SetWidth(_fLineWidth, _fLineWidth);
        _tLine.SetPosition(0, transform.position);
        _tLine.SetPosition(1, _tBuddy.position);
        _tLine.sortingOrder = 2;
    }


    public void StartFollow()
    {
        _bDragged = true;
        StartCoroutine(FollowMouse());
    }
    
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider.tag == "FallCheck" )
        {
            if (_iUsedOnTurn > 0)
                GameManager.TowerFall(_iUsedOnTurn );
        }

        if (!_bDragged)
            return;

        if (_tCollider.IsTouchingLayers(5))
        {
            _bDragged = false;
            //GameManager.UseSymbol(gameObject);
        }
    }
    
    public void Drop()
    {
        _bDragged = false;
        Debug.Log(name + " dropped!");
        GetComponent<Rigidbody2D>().gravityScale = 1f;
        //GetComponent<BoxCollider2D>().enabled = true;
    }
    public void Cancel()
    {
        _bDragged = false;
        Debug.Log(name + " cancelled!");
    }
    

    IEnumerator FollowMouse()
    {
        yield return null;

        while (_bDragged)
        {
            Vector3 tMousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
            Vector3 tMouseDir = (tMousePos - transform.position);//.normalized;
            _tRigidbody.velocity *= 0.98f;
            _tRigidbody.AddForce(tMouseDir * _fFollowForce);
           
            yield return null;
        }

    }
    public void DisapearSpinningStyle()
    {
        _tRigidbody.gravityScale = 0f;
        _tRigidbody.AddTorque(Random.Range(200f, 250f));
        _tRigidbody.AddForce(Random.Range(30f, 50f)*(Vector2)Random.onUnitSphere );
        _tCollider.enabled = false;
        StartCoroutine(ScaleDown(0.8f));
    }

    IEnumerator ScaleDown(float fDuration)
    {
        fDuration *= Random.Range(0.9f, 1.3f);
        Vector3 fScaleFactor = transform.localScale / fDuration;
        while (fDuration > 0f)
        {
            float fScale = _tScaleDown.Evaluate(1 - fDuration);
            transform.localScale = new Vector3(fScale, fScale, fScale);
            fDuration -= Time.deltaTime;
            yield return null;
        }
        Destroy(gameObject);
    }
}
