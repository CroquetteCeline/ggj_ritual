﻿using UnityEngine;
using System.Collections;

public class SpriteAnimation : MonoBehaviour {

	MeshRenderer BodyMeshRenderer;
	MeshFilter BodyMeshFilter;
	Mesh BodyMesh;
	Vector3[] Vertices;
	Vector3[] SmoothVertices;
	Vector2[] Uvs;
	int[] Tris;
	public Texture[] Turnaround;

	public Noisy VoiceHandler;

	public bool NPC =true;

	float Height;
	float Width;

	Vector3[] LeftSideVertices;
	Vector3[] RightSideVertices;

	Vector3[] LeftSideVerticesSmooth;
	Vector3[] RightSideVerticesSmooth;

	public Rigidbody2D TopRB;
	public Rigidbody2D CenterRB;
	public Rigidbody2D BottomLeftRB;
	public Rigidbody2D BottomRightRB;

	public Material BodyMat;

	Vector3 OffsetPos;

	float destination;
	public float Destination
	{
		get
		{
			return destination;
		}
		set
		{
			destination=value;
			if(!GoingToDestination)StartCoroutine(Moving());
		}
	}

	bool GoingToDestination = false;

	int direction =0;
	int Direction
	{
		get
		{
			return direction;
		}
		set
		{
			direction = value;
			GetUvsOnly();
			BodyMeshRenderer.material.mainTexture = Turnaround[value==0?0:1];
		}
	}

	void Awake () 
	{
		OffsetPos = transform.position;

		Height = TopRB.transform.localPosition.y*2;
		Width = BottomLeftRB.transform.localPosition.x-BottomRightRB.transform.localPosition.x;

		BodyMeshFilter = gameObject.AddComponent<MeshFilter>();
		BodyMeshRenderer = gameObject.AddComponent<MeshRenderer>();
		BodyMesh = new Mesh();
		BodyMeshRenderer.material=BodyMat;
		BodyMeshRenderer.useLightProbes=false;
		BodyMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		BodyMeshRenderer.reflectionProbeUsage= UnityEngine.Rendering.ReflectionProbeUsage.Off;
		
		LeftSideVertices = new Vector3[3];
		RightSideVertices = new Vector3[3];

		GetSidesVertices();
		GetTrisAndUvs ();
		if(NPC)StartCoroutine(TestMoving());
	}


	void Update()
	{
		if(!NPC)
		{
			if(Input.GetKeyUp("up"))
			{
				CenterRB.AddForce(Vector2.up*1000);
			}
			if(Input.GetKey("down"))
			{
				//Talking ();
				//Walking ();
				Trample();
			}
			if(Input.GetKey("left"))
			{
				BottomLeftRB.AddForce(-Vector2.right*25);
				CenterRB.AddForce(-Vector2.right*15);
			}
			if(Input.GetKey("right"))
			{
				BottomRightRB.AddForce(Vector2.right*25);
				CenterRB.AddForce(Vector2.right*15);
			}
		}
		



		if(Mathf.Abs(CenterRB.velocity.x)>3)
		{
			Direction = Mathf.RoundToInt(Mathf.Sign(CenterRB.velocity.x));
		}
		else if(Mathf.Abs(CenterRB.velocity.x)<1)
		{
			Direction=0;
		}
		
		Vector2 FootsDirection = (Vector2)(BottomRightRB.transform.position-BottomLeftRB.transform.position).normalized;

		Vector2 BottomToCenter = (Vector2)(CenterRB.transform.position-Vector3.Lerp(BottomLeftRB.transform.position,BottomRightRB.transform.position,0.5f)).normalized;
		Vector2 CenterToTop = (Vector2)(TopRB.transform.position-CenterRB.transform.position).normalized;

		Debug.DrawLine(CenterRB.transform.position,CenterRB.transform.position+(Vector3)BottomToCenter);
		Debug.DrawLine(TopRB.transform.position,TopRB.transform.position+(Vector3)CenterToTop);


		float Dot = Vector2.Dot(BottomToCenter,FootsDirection);
		float Dot90 = Vector2.Dot(BottomToCenter,new Vector2(-FootsDirection.y,FootsDirection.x));
		float ThumbDotDirection = Dot>0? (Dot90+1)*0.5f :-(Dot90+1)*0.5f;

		float TopDot = Vector2.Dot(CenterToTop,Vector2.right);
		float TopDot90 = Vector2.Dot(CenterToTop,-Vector2.up);
		float TopThumbDotDirection = TopDot>0? (TopDot90+1)*0.5f :-(TopDot90+1)*0.5f;

		CenterRB.AddTorque(ThumbDotDirection*5);
		TopRB.AddTorque(ThumbDotDirection*5);

		BottomLeftRB.AddForce(new Vector2(-BottomToCenter.y,BottomToCenter.x)*-ThumbDotDirection*10);
		BottomRightRB.AddForce(new Vector2(-BottomToCenter.y,BottomToCenter.x)*-ThumbDotDirection*10);
		CenterRB.AddForce(new Vector2(-BottomToCenter.y,BottomToCenter.x)*ThumbDotDirection*10);
		TopRB.AddForce(new Vector2(-CenterToTop.y,CenterToTop.x)*TopThumbDotDirection*10);
		TopRB.AddForce(Vector2.right*(CenterRB.transform.position.x-TopRB.transform.position.x)*40);

		//CenterRB.AddTorque(Dot*5);
		//TopRB.AddTorque(TopDot*5);

		BottomLeftRB.AddForce(Vector2.right*Mathf.Sin(Time.time*20)*CenterRB.velocity.x*25);
		BottomRightRB.AddForce(-Vector2.right*Mathf.Sin(Time.time*20)*CenterRB.velocity.x*25);
	}

	IEnumerator TestMoving()
	{

		while(true)
		{
			yield return new WaitForSeconds(Random.Range(10,20));
			Destination = Random.Range(-10,10);
			yield return null;
		}
	}


	IEnumerator Moving ()
	{
		GoingToDestination = true;
		float TimeBeforeJumping=1;
		float TimeMovingStoped=Time.time;

		while(Mathf.Abs(CenterRB.transform.position.x-Destination)>2)
		{
			if(CenterRB.velocity.magnitude>1)
			{
				TimeMovingStoped=Time.time;
			}

			if(Time.time>TimeMovingStoped+TimeBeforeJumping)
			{
				CenterRB.AddForce(Vector2.up*1500);
				if(VoiceHandler !=null)
				{
					VoiceHandler.PlayNoise(Noisy.SoundsType.losing);
				}
			}

			if(CenterRB.transform.position.x-Destination<0)
			{
				BottomRightRB.AddForce(Vector2.right*25);
				CenterRB.AddForce(Vector2.right*15);
			}
			else
			{
				BottomLeftRB.AddForce(-Vector2.right*25);
				CenterRB.AddForce(-Vector2.right*15);
			}
			yield return null;
		}

		GoingToDestination = false;
	}

	public void Talking()
	{
		CenterRB.AddForce(Vector2.up*Mathf.Sin(Time.time*10)*50);
	}

	void Walking()
	{
		BottomLeftRB.AddForce(Vector2.right*Mathf.Sin(Time.time*20)*75);
		BottomRightRB.AddForce(-Vector2.right*Mathf.Sin(Time.time*20)*75);

	}

	public void Trample()
	{
		BottomLeftRB.AddForce(Vector2.up*Mathf.Sin(Time.time*20)*30);
		BottomRightRB.AddForce(-Vector2.up*Mathf.Sin(Time.time*20)*30);
	}

	void GetSidesVertices()
	{
		LeftSideVertices[0] = TopRB.transform.localPosition+OffsetPos-transform.position+(TopRB.transform.right*(Width/2));
		LeftSideVertices[1] = CenterRB.transform.localPosition+OffsetPos-transform.position+(CenterRB.transform.right*(Width/2));
		LeftSideVertices[2] = BottomLeftRB.transform.localPosition+OffsetPos-transform.position;

		RightSideVertices[0] = TopRB.transform.localPosition+OffsetPos-transform.position+(TopRB.transform.right*-(Width/2));
		RightSideVertices[1] = CenterRB.transform.localPosition+OffsetPos-transform.position+(CenterRB.transform.right*-(Width/2));
		RightSideVertices[2] = BottomRightRB.transform.localPosition+OffsetPos-transform.position;

		RightSideVerticesSmooth = SmoothingPath(SmoothingPath(SmoothingPath(RightSideVertices)));
		LeftSideVerticesSmooth = SmoothingPath(SmoothingPath(SmoothingPath(LeftSideVertices)));
		
	}

	IEnumerator RefreshShape()
	{
		while(true)
		{
			GetSidesVertices();
			for(int i =0;i<RightSideVerticesSmooth.Length*2;i++)
			{
				if(i<RightSideVerticesSmooth.Length)
				{
					Vertices[i] = LeftSideVerticesSmooth[i];
				}
				else
				{
					Vertices[i] = RightSideVerticesSmooth[i-RightSideVerticesSmooth.Length];
				}
			}
			BodyMesh.vertices = Vertices;
			yield return null;
		}
	}

	void GetUvsOnly()
	{
		for(int i =0;i<RightSideVerticesSmooth.Length*2;i++)
		{
			if(i<RightSideVerticesSmooth.Length)
			{
				Uvs[i] = new Vector2(0,1-1.0f/(RightSideVerticesSmooth.Length-1)*i);
			}
			else
			{
				Uvs[i] = new Vector2(Direction==0?1:Direction,1-1.0f/(RightSideVerticesSmooth.Length-1)*(i-RightSideVerticesSmooth.Length));
			}
		}
		BodyMesh.uv = Uvs;
	}

	void GetTrisAndUvs ()
	{
		Vertices = new Vector3[RightSideVerticesSmooth.Length*2];
		Uvs = new Vector2[RightSideVerticesSmooth.Length*2];
		Tris = new int[((RightSideVerticesSmooth.Length*2)-2)*3];
		//Debug.Log (Vertices.Length+" "+Uvs.Length+" "+Tris.Length);
		for(int i =0;i<RightSideVerticesSmooth.Length*2;i++)
		{
			if(i<RightSideVerticesSmooth.Length)
			{
				Vertices[i] = LeftSideVerticesSmooth[i];
				Uvs[i] = new Vector2(0,1-1.0f/(RightSideVerticesSmooth.Length-1)*i);
			}
			else
			{
				Vertices[i] = RightSideVerticesSmooth[i-RightSideVerticesSmooth.Length];
				Uvs[i] = new Vector2(1,1-1.0f/(RightSideVerticesSmooth.Length-1)*(i-RightSideVerticesSmooth.Length));
			}
		}



		for(int i =0;i<Tris.Length/6;i++)
		{
			Tris[i*6]=i;
			Tris[i*6+1]=i+Vertices.Length/2;
			Tris[i*6+2]=i+1;

			Tris[i*6+3]=i+1;
			Tris[i*6+4]=i+Vertices.Length/2;
			Tris[i*6+5]=i+Vertices.Length/2+1;

			//Debug.Log (i+" "+(i+Vertices.Length/2)+" "+(i+1)+" "+(i+1)+" "+(i+Vertices.Length/2)+" "+(i+Vertices.Length/2+1));
		}

		BodyMesh.vertices = Vertices;
		BodyMesh.uv = Uvs;
		BodyMesh.triangles= Tris;
		BodyMeshFilter.mesh = BodyMesh;
		StartCoroutine(RefreshShape());
	}

	Vector3[] SmoothingPath(Vector3[] PathToSmooth)
	{
		Vector3[] SmoothPath=new Vector3[PathToSmooth.Length*2-1];
		float distance;
		Vector3 previousDir;
		Vector3 nextDir;
		
		{
			//SmoothPath=new Vector3[PathToSmooth.Length*2];
			for(int i=0;i<PathToSmooth.Length-1;i++)
			{
				Vector3 pointplus1;
				
				if(i+2>PathToSmooth.Length-1)
				{
					pointplus1 = PathToSmooth[PathToSmooth.Length-1];
					nextDir = (PathToSmooth[PathToSmooth.Length-2] - pointplus1).normalized;
				}
				else
				{
					pointplus1 = PathToSmooth[i+1>PathToSmooth.Length-1?i+1-(PathToSmooth.Length):i+1];
					nextDir = (pointplus1 - PathToSmooth[(i+2)>PathToSmooth.Length-1?i+2-(PathToSmooth.Length):i+2]).normalized;
				}
				if(i-1==-1)
				{
					previousDir = (PathToSmooth[1]-PathToSmooth[0]).normalized;
				}
				else
				{
					previousDir = (PathToSmooth[i]-PathToSmooth[i-1==-1?PathToSmooth.Length-1:i-1]).normalized;
				}
				
				distance = (PathToSmooth[i]-pointplus1).magnitude/4;
				
				
				SmoothPath[i*2]= PathToSmooth[i];
				SmoothPath[i*2+1]= ((PathToSmooth[i]+previousDir*distance)+(pointplus1+nextDir*distance))/2;
				
				
			}
			SmoothPath[SmoothPath.Length-1] = PathToSmooth[PathToSmooth.Length-1];
			PathToSmooth = new Vector3[SmoothPath.Length];
			PathToSmooth = SmoothPath;
		}
		
		return SmoothPath;
	}
}
