﻿using UnityEngine;
using System.Collections;

public class Paralax : MonoBehaviour
{
    public Transform[] Layers;
	public float MinWeight;
	public float MaxWeight;



    Transform _tCamera;

    void Start ()
    {
        _tCamera = GameObject.Find("Main Camera").transform;

		for(int i=0;i<Layers.Length;i++)
		{
			Layers[i].position = Vector3.forward*((Layers.Length-i)*4);
		}
    }
	

	void Update ()
    {
        Vector3 tCamSnap = _tCamera.position;
        tCamSnap.y = 0f;

		Vector3 tempPos;

		for(int i=0;i<Layers.Length;i++)
		{
			tempPos.z = Layers[i].position.z;
			Layers[i].position = Vector3.Lerp(tCamSnap, transform.position, MinWeight+i*(MaxWeight-MinWeight)/(Layers.Length-1));
			tempPos.x = Layers[i].position.x;
			tempPos.y = Layers[i].position.y;
			Layers[i].position = tempPos;
		}
    }
}
