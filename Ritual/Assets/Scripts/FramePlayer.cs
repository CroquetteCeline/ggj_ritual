﻿using UnityEngine;
using System.Collections;

public class FramePlayer : MonoBehaviour
{
    Transform _tPlayer;
    float _fCameraWidth;
    public float _fFrameRatioLimit = 0.2f;
    float _fLeftLimit;
    float _fRightLimit;
    public float _fMoveSpeed = 0.1f;

    void Start ()
    {
        _tPlayer = GameObject.Find("ViewFocus").transform;
        _fCameraWidth = Camera.main.pixelWidth;

        _fLeftLimit = _fCameraWidth * _fFrameRatioLimit;
        _fRightLimit = _fCameraWidth - _fLeftLimit;

        transform.position = new Vector3( _tPlayer.position.x, transform.position.y, transform.position.z );

    }
	

	void Update ()
    {
        if (GameManager.DialogState != DialogState.Hidden)
            return;

        Vector3 tScrPos = Camera.main.WorldToScreenPoint(_tPlayer.transform.position);

        //Debug.Log(tScrPos);

        if (tScrPos.x < _fLeftLimit)
        {
            transform.Translate(Vector3.left * _fMoveSpeed * (_fLeftLimit - tScrPos.x) * Time.deltaTime);
        }
        else if (tScrPos.x > _fRightLimit)
        {
            transform.Translate(Vector3.right * _fMoveSpeed * (tScrPos.x - _fRightLimit) * Time.deltaTime);
        }

    }
    
}
