﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
    public float _fMoveSpeed = 2f;
    Vector3 _fFocusDistance;
    Transform _tFocus;
    NPC _tLastNPC = null;

	void Start ()
    {
        _tFocus = GameObject.Find("ViewFocus").transform;
        _fFocusDistance = _tFocus.localPosition;
    }

	void Update ()
    {
        if (GameManager.DialogState != DialogState.Hidden)
            return;

        if (Input.GetKeyDown(KeyCode.Space) && _tLastNPC != null )
        {
            GameManager.StartDialog(_tLastNPC);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(Vector3.left * _fMoveSpeed * Time.deltaTime);
            _tFocus.localPosition = -_fFocusDistance;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector3.right * _fMoveSpeed * Time.deltaTime);
            _tFocus.localPosition = _fFocusDistance;
        }
    }

    void OnTriggerEnter2D(Collider2D tOther)
    {
        if (tOther.tag == "NPC")
        {
            _tLastNPC = tOther.GetComponent<NPC>();
        }
    }
    void OnTriggerExit2D(Collider2D tOther)
    {
        if (_tLastNPC == tOther.GetComponent<NPC>())
        {
            _tLastNPC = null;
        }
    }

    void OnTriggerStay2D(Collider2D tOther)
    {
        //Debug.Log(tOther + "triggerstay debug");
        if (tOther.tag == "NPC")
        {
            if (tOther.tag == "NPC")
            {
				_tLastNPC.StartWaving();
                _tLastNPC = tOther.GetComponent<NPC>();
            }
                
        }
    }
}
