﻿using UnityEngine;
using System.Collections.Generic;

public enum DialogState
{
    Hidden,
    Start,
    Player,
    NPC,
    Wait,
    Win,
    Lose,
}

public class Dialog : MonoBehaviour
{
    public List<NPC_Line> _Lines = new List<NPC_Line>();
    public int _iTop = 5;
    public Symbol _eReward;
}
