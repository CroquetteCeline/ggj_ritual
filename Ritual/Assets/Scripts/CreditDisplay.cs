﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreditDisplay : MonoBehaviour 
{
    public Text _UI_item;
    public float _fFadeDuration;
	
	void Start () 
    {
        _UI_item.CrossFadeAlpha(0f, 0f, true);
    }
	
	void Update () 
    {}

    void OnTriggerEnter2D(Collider2D tOther)
    {
        if (tOther.tag == "Player")
        {
            StopAllCoroutines();
            _UI_item.CrossFadeAlpha(1f, _fFadeDuration, true);
            //StartCoroutine(FadeAlphaTo(1f));
        }
    }
    void OnTriggerExit2D(Collider2D tOther)
    {
        if (tOther.tag == "Player")
        {
            StopAllCoroutines();
            _UI_item.CrossFadeAlpha(0f, _fFadeDuration, true);
        }
    }

    IEnumerator FadeAlphaTo(float fAlphaValue)
    {
        float fDir = 1f;

        if (fAlphaValue < _UI_item.color.a )
        {
            fDir = -1f;
        }

        while (fAlphaValue * fDir < _UI_item.color.a * fDir)
        {

            yield return null;
        }

        _UI_item.color = new Vector4( _UI_item.color.r, _UI_item.color.g, _UI_item.color.b, fAlphaValue );
    }

}
