﻿using UnityEngine;
using System.Collections;

public enum Mood
{
    Happy,
    Okay,
    Neutral,
    Bored,
    Angry, 
    Sad,
}

public class NPC : MonoBehaviour
{
    public Dialog _tDialog;
    public Mood _eMood = Mood.Neutral;
    public float _fAttitude = 5f;

    public void SetMood(Mood eNew)
    {
        switch (eNew)
        {
            case Mood.Angry:
                AngryReaction();
                _fAttitude -= 5f;
                break;
            case Mood.Bored:
                _fAttitude -= 1f;
                break;
            case Mood.Happy:
                _fAttitude += 10f;
                break;
            case Mood.Okay:
                _fAttitude += 2f;
                break;
            case Mood.Sad:
                _fAttitude -= 3f;
                break;
            case Mood.Neutral:
                _fAttitude *= 0.66f;
                break;
            default:
                break;
        }
        _eMood = eNew;

        Debug.Log(_eMood);
    }

    void AngryReaction()
    {
        GameManager.HitGame( _fAttitude);
    }

    public void StartWaving()
    {
		gameObject.GetComponent<SpriteAnimation>().Talking();
    }
}
