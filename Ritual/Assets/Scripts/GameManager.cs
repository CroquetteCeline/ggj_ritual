﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    private static GameManager _tInstance;
    public GameObject _tHomeScreen;

    //Dialog Game
    [SerializeField]
    private DialogState _eDialogState;
    public static DialogState DialogState { get { return _tInstance._eDialogState; } }
    static int _iTurn = -1;
    static GameObject _tDialogDisplay;
    static Dialog _tCurrentDialog = null;
    static Transform _tLineToReach;
    NPC _tCurrentCharacter = null;
    //static bool _bWin = false;

    //PhysicItems
    static List<GameObject> _tSymbolsDropped_Player = new List<GameObject>();
    static List<GameObject> _tSymbolsDropped_NPC = new List<GameObject>();
    int _iNextLinkValue = 0;
    //public Transform _tPlayerAxis;

    // IA Spawn Ref
    public Transform _tNPCAxis;
    public float _fSymbolOffsetY = 0.7f;

    //Player Interaction zone
    static VocabLibrary _tVocab;
    public Transform _tSwipeZone; //trigger
    static GameObject _tSymbolFallingOnHead = null;

    //Physic Coroutine
    public static float _fStillDurationExpected = 1f;
    public static GameObject _tSymbolOnMouse = null;
    public Collider2D _tNoDragArea;
    public Transform _tSymbolDragOrigin;
    public Transform _tLastSelection;
  
    static List<GameObject> _tIconsChoice = new List<GameObject>();
    public int _iIconIDFramed = 0; //trigger
    //public Vector3 _tMouseContact;

    bool _bHomeScreen = true;

    //Player
    List<Symbol> _tSymbolsAvailable = new List<Symbol>();

    void Start ()
    {
        _tInstance = this;

        _eDialogState = DialogState.Hidden;
        _tDialogDisplay = GameObject.Find("Dialog");
        _tLineToReach = GameObject.Find("HeigthToReach").transform;
        //_tVocab = GameObject.Find("Dialog/Vocab").GetComponent<VocabLibrary>();      
        _tSwipeZone = GameObject.Find("Vocab_next").transform;

        _tDialogDisplay.SetActive(false);

        DiscoverSymbol(Symbol.Happy);
        DiscoverSymbol(Symbol.NotHappy);
        DiscoverSymbol(Symbol.NoHand);
		DiscoverSymbol(Symbol.Question);
		DiscoverSymbol(Symbol.Exclamation);
    }	
	void Update ()
    {
        if (_bHomeScreen && Input.GetMouseButtonDown(0))
        {
            _tInstance._tHomeScreen.SetActive(false);
            _bHomeScreen = false;
        }


        switch (_eDialogState)
        {
            /*
            case DialogState.Hidden:
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    StartDialog(_tCharacters[0]);
                }
                break;
                */
            case DialogState.Player:
                CheckMouseRelease();
                
                /*
                if (Input.GetKeyDown(KeyCode.Alpha1))
                    UseSymbol(0);
                    */
                
                break;

            default:
                break;
        }
	}

    public static void StartDialog(NPC tNPC)
    {
        if ( DialogState != DialogState.Hidden)
            return;

        _tInstance._tCurrentCharacter = tNPC;
        Debug.Log(" Start Dialog with NPC " + tNPC.name);
        _tInstance._eDialogState = DialogState.NPC;      
        _tDialogDisplay.SetActive(true);
        _iTurn = -1;
        _tCurrentDialog = tNPC._tDialog;

        for (int i = 0; i < _tInstance._tSymbolsAvailable.Count; ++i)
        {
            GameObject tNew = SpawnSymbolTile(_tInstance._tSymbolsAvailable[i], true);
            _tIconsChoice.Add(tNew);
            tNew.transform.parent = _tInstance._tSwipeZone;
            tNew.transform.localPosition = Vector3.zero;
            tNew.GetComponent<Collider2D>().enabled = false;

            if (i > 0)
                tNew.GetComponent<SpriteRenderer>().enabled = false;
        }
        
        _tLineToReach.localPosition = (new Vector3(0f, (_tCurrentDialog._iTop -1)* 1.4f, 0f));
        _tInstance.StartNPCTurn();
    }
    public static IEnumerator EndDialog( bool bWin)
    {
        if ( bWin)
        {
            _tInstance.DiscoverSymbol(_tCurrentDialog._eReward);
        }
        else {

        }

        Debug.Log(" End Dialog with NPC ");
        _tInstance._eDialogState = DialogState.Hidden;

        yield return new WaitForSeconds(2f);
       
        _tInstance._tCurrentCharacter = null;
        _tDialogDisplay.SetActive(false);
        _tCurrentDialog = null;
        _tLineToReach.localPosition = Vector3.zero;

        ClearAllSymbolTiles();
    }

    IEnumerator DropSymbol()
    {
        DialogState eNext = DialogState.Wait;
        Rigidbody2D tCurrentSymbolRB = null;

        switch (_eDialogState)
        {
            case DialogState.Player:
                tCurrentSymbolRB = _tSymbolsDropped_Player[_iTurn].GetComponent<Rigidbody2D>();
                eNext = DialogState.NPC;
                
                
                break;

            case DialogState.NPC:
                tCurrentSymbolRB = _tSymbolsDropped_NPC[_iTurn].GetComponent<Rigidbody2D>();
                eNext = DialogState.Player;
                break;
        }

        float fLastTimeMvt = Time.time;
        yield return null;
        while (Time.time < fLastTimeMvt + _fStillDurationExpected)
        {
            if (tCurrentSymbolRB != null && (tCurrentSymbolRB.velocity.sqrMagnitude > 0.1f || Mathf.Abs(tCurrentSymbolRB.angularVelocity) > 0.1f) )
            {
                fLastTimeMvt = Time.time;
            }
            yield return null;
        }

        //Debug.Log(_eDialogState);
        //_eDialogState = eNext;

        if (eNext == DialogState.NPC)
        {
            if (_iTurn <= _tSymbolsDropped_NPC.Count - 1 )
                _tSymbolsDropped_Player[_iTurn].GetComponent<PuzzleSymbol>().CreateLink(_tSymbolsDropped_NPC[_iTurn], _tInstance._tCurrentCharacter._eMood );

            StartNPCTurn();
        }
        else
            StartPlayerTurn();
    }
    void StartNPCTurn()
    {
        _eDialogState = DialogState.NPC;
        

        if (_tCurrentDialog == null)
        {
            _eDialogState = DialogState.Hidden;
            return;
        }

        _iTurn += 1;

        if (_iTurn > _tCurrentDialog._Lines.Count -1)
        {
            StartCoroutine( EndDialog(true));
            return;
        }

        Symbol eSymbolUsed = _tCurrentDialog._Lines[_iTurn]._eSymbol;
        Debug.Log("NPC uses " + eSymbolUsed);

        GameObject tNew = SpawnSymbolTile(eSymbolUsed, false);
        tNew.GetComponent<PuzzleSymbol>()._iUsedOnTurn = _iTurn;
        Vector3 tSpawnPos;

        if (_iTurn > 0)
            tSpawnPos = _tSymbolsDropped_NPC[_iTurn - 1].transform.position + new Vector3(0f, _fSymbolOffsetY);
        else
            tSpawnPos = _tNPCAxis.position + new Vector3(0f, _fSymbolOffsetY) * _iTurn;

        tNew.transform.position = tSpawnPos;
        _tSymbolsDropped_NPC.Add(tNew);

        Debug.Log("start coroutine fall _npc");
        StartCoroutine( DropSymbol() );
    }
    void StartPlayerTurn()
    {
        _eDialogState = DialogState.Player;
        ShowBubble( true ); //enable swipe zone
    }

    void ShowBubble( bool bShow )
    {
        _tSwipeZone.gameObject.SetActive(bShow);
    }

    public static void UseSymbol(GameObject tSymbolUsed)
    {
        Symbol eSymbolUsed = tSymbolUsed.GetComponent<PuzzleSymbol>()._eType;

        if (eSymbolUsed == _tCurrentDialog._Lines[_iTurn]._eBestAnswer)
            _tInstance._tCurrentCharacter.SetMood( Mood.Happy );
        
        else if (eSymbolUsed == _tCurrentDialog._Lines[_iTurn]._eNiceAnswer)       
            _tInstance._tCurrentCharacter.SetMood(Mood.Okay);
        
        else if (eSymbolUsed == _tCurrentDialog._Lines[_iTurn]._eBadAnswer)     
            _tInstance._tCurrentCharacter.SetMood(Mood.Angry);
        
        else
            _tInstance._tCurrentCharacter.SetMood(Mood.Bored);

        _tSymbolsDropped_Player.Add(tSymbolUsed);
        PuzzleSymbol tPuzzle = tSymbolUsed.GetComponent<PuzzleSymbol>();
        tPuzzle._iUsedOnTurn = _iTurn;

        Debug.Log("start coroutine fall _player :" + eSymbolUsed );
        _tInstance.StartCoroutine(_tInstance.DropSymbol());
    }

    static GameObject SpawnSymbolTile(Symbol eS, bool bVocab)
    {
        GameObject tNew = Instantiate(Resources.Load("Symbols/" + eS.ToString(), typeof(GameObject))) as GameObject;
        if (bVocab)
        {
            tNew.GetComponent<Rigidbody2D>().gravityScale = 0f;
        }
        //tNew.GetComponent<PuzzleSymbol>()._bClickable = bVocab;
        return tNew;
    }
    public static void Drag(SwipeDir eDir)
    {
        if (!PlayerInputAllowed())
            return;

        Debug.Log(eDir);

        switch (eDir)
        {
            //case SwipeDir.Left:
            //break;
            case SwipeDir.Right:
                GameObject tClone = Instantiate(_tIconsChoice[_tInstance._iIconIDFramed]);
                tClone.transform.position = _tIconsChoice[_tInstance._iIconIDFramed].transform.position;
                tClone.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
                _tSymbolOnMouse = tClone;
                tClone.GetComponent<Collider2D>().enabled = true;
                //tClone.GetComponent<Rigidbody2D>().gravityScale = 1f;
                tClone.GetComponent<PuzzleSymbol>().StartFollow();
                _tInstance.ShowBubble(false);

                break;
            case SwipeDir.Up:
                ChangeIconFrame(-1);
                break;
            case SwipeDir.Down:
                ChangeIconFrame(1);
                break;
        }

    }
    void CheckMouseRelease()
    {
        if (_tSymbolOnMouse != null && PlayerInputAllowed() )
        {
            if (Input.GetMouseButtonUp(0))
            {                
                PuzzleSymbol tPiece = _tSymbolOnMouse.GetComponent<PuzzleSymbol>();

                tPiece.Drop();
                UseSymbol(_tSymbolOnMouse);
                _tSymbolOnMouse = null;             
            }
        }
    }

    static void DropSymbolOnPlayer()
    {
        _tSymbolFallingOnHead = Instantiate(_tIconsChoice[_tInstance._iIconIDFramed]);
        _tSymbolFallingOnHead.transform.position = _tIconsChoice[_tInstance._iIconIDFramed].transform.position;
        _tSymbolFallingOnHead.transform.localScale = new Vector3(1f, 1f, 1f);
        _tSymbolFallingOnHead.GetComponent<Collider2D>().enabled = true;
        _tSymbolFallingOnHead.GetComponent<Rigidbody2D>().gravityScale = 1f;

        _tInstance.ShowBubble(false);
    }

    public static bool PlayerInputAllowed()
    {
        if ( DialogState != DialogState.Player || _tSymbolsDropped_Player.Count -1 >= _iTurn )
            return false;

        return true;
    }
    public static void HitGame(float fAttitude)
    {
        if (_iTurn < 1)
            return;

        Debug.Log("Rage up to " + fAttitude);
        _tSymbolsDropped_NPC[0].GetComponent<Rigidbody2D>().AddForce(Vector3.up * -fAttitude);
        _tSymbolsDropped_Player[0].GetComponent<Rigidbody2D>().AddForce(Vector3.up * -fAttitude);
    }
    public static void TowerFall( int iTurn )
    {
        //Debug.Log(iTurn + " touched ?");
        //DropSymbolOnPlayer();
        _tInstance.StartCoroutine(EndDialog(false));
    }
    void DiscoverSymbol(Symbol eSymbol)
    {
        _tSymbolsAvailable.Add(eSymbol);
    }

    static void ChangeIconFrame(int iAjust)
    {
        _tIconsChoice[_tInstance._iIconIDFramed].GetComponent<SpriteRenderer>().enabled = false;
        _tInstance._iIconIDFramed = (_tInstance._iIconIDFramed + iAjust + _tIconsChoice.Count) % _tIconsChoice.Count;
        _tIconsChoice[_tInstance._iIconIDFramed].GetComponent<SpriteRenderer>().enabled = true;
    }

    static void ClearAllSymbolTiles()
    {
        if (_tSymbolFallingOnHead != null)
        {
            _tSymbolFallingOnHead.GetComponent<PuzzleSymbol>().DisapearSpinningStyle();
            _tSymbolFallingOnHead = null;
        }

        if (_tSymbolOnMouse != null)
        {
            _tSymbolOnMouse.GetComponent<PuzzleSymbol>().DisapearSpinningStyle();
            _tSymbolOnMouse = null;
        }

        for (int i = 0; i < _tSymbolsDropped_NPC.Count; ++i)
        {
            _tSymbolsDropped_NPC[i].GetComponent<PuzzleSymbol>().DisapearSpinningStyle();
        }
        for (int i = 0; i < _tSymbolsDropped_Player.Count; ++i)
        {
            _tSymbolsDropped_Player[i].GetComponent<PuzzleSymbol>().DisapearSpinningStyle();
        }

        _tSymbolsDropped_NPC.Clear();
        _tSymbolsDropped_Player.Clear();
    }
}
