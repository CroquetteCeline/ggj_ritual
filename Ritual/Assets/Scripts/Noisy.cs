﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class Noisy : MonoBehaviour {

	AudioSource Voice;

	public AudioClip[] WinSounds;
	public AudioClip[] LoseSounds;
	public AudioClip[] LikeSounds;
	public enum SoundsType {Winning,losing,liking};

	void Awake () 
	{
		Voice = gameObject.AddComponent<AudioSource>();
        
        AudioMixer mixer = Resources.Load("Sounds/Mixer") as AudioMixer;
        Voice.outputAudioMixerGroup = mixer.FindMatchingGroups("NPCs")[0];
	}

	public void PlayNoise(SoundsType SoundsToMake)
	{
		switch(SoundsToMake)
		{
		case SoundsType.Winning:
			Voice.clip= WinSounds[Random.Range(0,WinSounds.Length)];
			break;
		
		case SoundsType.losing:
			Voice.clip= LoseSounds[Random.Range(0,LoseSounds.Length)];
			break;

		case SoundsType.liking:
			Voice.clip= LikeSounds[Random.Range(0,LikeSounds.Length)];
			break;
		}
		if(Voice.clip != null)Voice.Play();

	}
	void Update () {
	
	}
}
