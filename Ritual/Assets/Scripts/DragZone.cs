﻿using UnityEngine;
using System.Collections;

public enum SwipeDir
{
    Left, Right, Up, Down, None,
}

public class DragZone : MonoBehaviour
{
    bool _bTouch = false;
    Vector3 _tTouchScreenPosLast;
    public float _fSwipeMin = 50f;

	void Start ()
    {
	
	}

    void OnMouseDown()
    {
        _bTouch = true;
        _tTouchScreenPosLast = Input.mousePosition;
    }

    void Update ()
    {
        if (_bTouch )//&& Input.GetMouseButtonUp(0))
        {
            Vector3 tSwipeDir = Input.mousePosition - _tTouchScreenPosLast;
            SwipeDir eDir = SwipeDir.None;

            if (tSwipeDir.x < -_fSwipeMin)
                eDir = SwipeDir.Left;

            else if (tSwipeDir.x > _fSwipeMin)
                eDir = SwipeDir.Right;

            else if (tSwipeDir.y > _fSwipeMin)
                eDir = SwipeDir.Up;

            else if (tSwipeDir.y < - _fSwipeMin)
                eDir = SwipeDir.Down;
            else
                return;

            _bTouch = false;

            GameManager.Drag(eDir);
        }
	}
}
